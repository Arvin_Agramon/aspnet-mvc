﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Flier.DAL;
using Flier.Models;

namespace Flier.BOL
{
    public class ProductObject
    {
        DataAccessObjects dao = new DataAccessObjects("");
        public DataTable getProducts()
        {            
            DataTable dt = dao.GetProducts();
            
            return dt;
        }

        public string insertProduct(string name, string desc, int price)
        {
            int? check = dao.InsertProduct(name, desc, price);

            if (check > 0)
                return "Insert success";
            else
                return "Error occured";
        }

        public string updateProduct(string name, string desc, int price, int prodId)
        {
            int? check = dao.UpdateProduct(prodId, name, desc, price);

            if (check > 0)
                return "Update success";
            else
                return "Error occured";
        }

        public string deleteProduct(int prodId)
        {
            int? check = dao.DeleteProduct(prodId);

            if (check > 0)
                return "Delete success";
            else
                return "Error occured";

        }
    }
}