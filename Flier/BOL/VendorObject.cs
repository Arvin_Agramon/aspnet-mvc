﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Flier.DAL;
using Flier.Models;

namespace Flier.BOL
{
    public class VendorObject
    {
        DataAccessObjects dao = new DataAccessObjects("");
        public DataTable getVendors()
        {
            
            DataTable dt = dao.GetVendors();
            return dt;
        }

        public DataTable getVendorProducts(int vendorId)
        {
            DataTable dt = dao.getVendorProducts(vendorId);
            return dt;
        }

        public List<ProductViewModel> getListedVendorProducts(int vendorId)
        {
            List<ProductViewModel> prod = new List<ProductViewModel>();

            DataTable dt = getVendorProducts(vendorId);

            prod = (from DataRow dr in dt.Rows
                    select new ProductViewModel
                    {
                        ProductID = Convert.ToInt32( dr["ProductID"]),
                        Name = dr["Name"].ToString(),
                        Description = dr["Description"].ToString()
                    }).ToList();

            return prod;
        }

        public string updateVendor(int vendorId, string name, string desc)
        {
            int? check =
            dao.UpdateVendor(vendorId, name, desc);

            if (check > 0)
                return "Update success";
            else
                return "Error occured";
        }

        public string insertVendor(string name, string desc)
        {
            int? check = dao.InsertVendor(name, desc);

            if (check > 0)
                return "Insert success";
            else
                return "Error occured";
        }

        public string deleteVendor(int vendorid)
        {
            int? check = dao.DeleteVendor(vendorid);

            if (check > 0)
                return "Delete success";
            else
                return "Error occured";
        }

        public string insertVendorProduct(string name, string desc, int price, int vendorid)
        {
            int check = dao.InsertVendorProduct(name, desc, price, vendorid);

            if (check > 0)
                return "Insert success";
            else
                return "Error occured";
        }


    }
}