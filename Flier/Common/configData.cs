﻿using System;
using System.Configuration;

namespace Flier.Common
{
    public class configData
    {

        public static DateTime GetDueDate()
        {
            DateTime dueDate = Convert.ToDateTime(ConfigurationManager.AppSettings["DueDate"]);

            return dueDate;
        }


    }
}