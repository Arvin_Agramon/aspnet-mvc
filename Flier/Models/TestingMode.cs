﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Flier.Models
{
    public class TestingModel
    {
        public string ThisIs { get; set; }
        public string Junk { get; set; }

    }


    public class HomeModel
    {
        [DisplayFormat(DataFormatString = "{0:f}")]
        public DateTime DueDate { get; set; }
    }
}