﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Flier.Models
{
    public class VendorViewModel
    {
        public int VendorID { get; set; }
        [Required]
        [Display(Name = "Vendor")]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<ProductViewModel> Products { get; set; }
    }
}