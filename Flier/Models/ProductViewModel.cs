﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Flier.Models
{
    public class ProductViewModel
    {
        public int ProductID { get; set; }
        [Required]
        [Display(Name = "Product Name")]
        public string Name { get; set; }

        public string Description { get; set; }
        public int Price { get; set; }
    }
}