﻿using System;
using System.Data;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Flier.Models;
using Flier.BOL;

namespace Flier.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            DateTime due = Common.configData.GetDueDate();

            HomeModel model = new HomeModel();
            model.DueDate = due;

            return View(model);
        }

        public ActionResult Product()
        {
            ProductObject prod = new ProductObject();
            DataTable dt = prod.getProducts();
            List<ProductViewModel> prodList = new List<ProductViewModel>();
            prodList = (from DataRow dr in dt.Rows
                        select new ProductViewModel()
                        {
                            ProductID = Convert.ToInt32(dr["ProductID"]),
                            Name = dr["Name"].ToString(),
                            Description = dr["Description"].ToString(),
                            //Price = Convert.ToInt32(dr["Price"])
                        }).ToList();

            return View(prodList);
        }

        public ActionResult Vendor()
        {
            VendorObject vend = new VendorObject();
            DataTable dt = vend.getVendors();
            List<VendorViewModel> prodList = new List<VendorViewModel>();
            prodList = (from DataRow dr in dt.Rows
                        select new VendorViewModel()
                        {
                            VendorID = Convert.ToInt32(dr["VendorID"]),
                            Name = dr["Name"].ToString(),
                            Description = dr["Description"].ToString(),
                            Products = vend.getListedVendorProducts(Convert.ToInt32(dr["VendorID"]))
                        }).ToList();
            
            ViewBag.Message = "Contact me anytime";

            return View(prodList);
        }

        public ActionResult Arvin()
        {
            ViewBag.Title = "Custom page";

            return View();
        }

        [HttpPost]
        public JsonResult UpdateProduct(string productid, string name, string description, string price)
        {
            ProductObject prod = new ProductObject();
            string ret = prod.updateProduct(name, description, Convert.ToInt32(price), Convert.ToInt32(productid));

            return Json(ret);
        }

        [HttpPost]
        public JsonResult InsertProduct(string name, string description, string price)
        {
            ProductObject prod = new ProductObject();
            string ret = prod.insertProduct(name, description, Convert.ToInt32(price));

            return Json(ret);
        }

        [HttpPost]
        public JsonResult DeleteProduct(string productid)
        {
            ProductObject prod = new ProductObject();
            string ret = prod.deleteProduct(Convert.ToInt32(productid));

            return Json(ret);
        }

        [HttpPost]
        public JsonResult UpdateVendor(string vendorid, string name, string description)
        {
            VendorObject prod = new VendorObject();
            string ret = prod.updateVendor( Convert.ToInt32(vendorid),name,description);

            return Json(ret);
        }

        [HttpPost]
        public JsonResult InsertVendor(string name, string description)
        {
            VendorObject prod = new VendorObject();
            string ret = prod.insertVendor(name, description);

            return Json(ret);
        }

        [HttpPost]
        public JsonResult DeleteVendor(string vendorid)
        {
            VendorObject prod = new VendorObject();
            string ret = prod.deleteVendor(Convert.ToInt32(vendorid));

            return Json(ret);
        }

        [HttpPost]
        public JsonResult InsertVendorProduct(string name, string description, string price, string vendorid)
        {
            VendorObject vend = new VendorObject();
            string ret = vend.insertVendorProduct(name, description, Convert.ToInt32(price), Convert.ToInt32(vendorid));

            return Json(ret);
        }

    }
}