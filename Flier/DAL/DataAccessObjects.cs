﻿using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Flier.DAL
{
    public class DataAccessObjects
    {
        public DataAccessObjects(string connectionString)
        {
            if (String.IsNullOrEmpty(connectionString))
                connectionString = ConfigurationManager.ConnectionStrings["local"].ConnectionString; 

            ConnectionString = connectionString;
        }

        #region Products
        public DataTable GetProducts()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlDataReader sdr;
            SqlCommand cmd = new SqlCommand();
            DataTable dtProducts = new DataTable("Products");

            try
            {
                

                cmd.CommandText = "SELECT * FROM Products";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();

                sdr = cmd.ExecuteReader();
                dtProducts.Load(sdr);

                con.Close();
                return dtProducts;
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int? InsertProduct(string Name, string Desc, int Price)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "INSERT INTO Products (Name, [Description], DateAdded) VALUES(@Name, @Desc, GETDATE())";
                
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                cmd.Parameters["@Name"].Value = Name;
                cmd.Parameters.Add("@Desc", SqlDbType.NVarChar);
                cmd.Parameters["@Desc"].Value = Desc;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception)
            { throw; }


            return ret;
        }
        
        public int? UpdateProduct(int productID, string Name, string Desc, int Price)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "UPDATE Products SET [Name]=@Name, [Description]=@Desc  WHERE ProductID=@ID";
                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = productID;
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                cmd.Parameters["@Name"].Value = Name;
                cmd.Parameters.Add("@Desc", SqlDbType.NVarChar);
                cmd.Parameters["@Desc"].Value = Desc;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch(Exception)
            { throw; }
            

            return ret;
        }

        public int? DeleteProduct(int productID)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "DELETE Products WHERE ProductID=@ID";
                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = productID;
                
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception)
            { throw; }


            return ret;
        }
        #endregion

        #region Vendors
        public DataTable GetVendors()
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlDataReader sdr;
            SqlCommand cmd = new SqlCommand();
            DataTable dtVendors = new DataTable("Vendors");

            try
            {
                cmd.CommandText = "SELECT * FROM Vendors";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();

                sdr = cmd.ExecuteReader();
                dtVendors.Load(sdr);

                con.Close();
                return dtVendors;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public int? InsertVendor(string Name, string Desc)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "INSERT INTO Vendors (Name, [Description], DateAdded) VALUES(@Name, @Desc, GETDATE())";

                cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                cmd.Parameters["@Name"].Value = Name;
                cmd.Parameters.Add("@Desc", SqlDbType.NVarChar);
                cmd.Parameters["@Desc"].Value = Desc;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception)
            { throw; }


            return ret;
        }

        public int? UpdateVendor(int vendorID, string Name, string Desc)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "UPDATE Vendors SET [Name]=@Name, [Description]=@Desc  WHERE VendorID=@ID";
                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = vendorID;
                cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                cmd.Parameters["@Name"].Value = Name;
                cmd.Parameters.Add("@Desc", SqlDbType.NVarChar);
                cmd.Parameters["@Desc"].Value = Desc;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception)
            { throw; }


            return ret;
        }

        public int? DeleteVendor(int vendorID)
        {
            int? ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "DELETE Vendors WHERE VendorID=@ID";
                cmd.Parameters.Add("@ID", SqlDbType.Int);
                cmd.Parameters["@ID"].Value = vendorID;

                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = cmd.ExecuteNonQuery();

                con.Close();
            }
            catch (Exception)
            { throw; }


            return ret;
        }

        public DataTable getVendorProducts(int vendorID)
        {
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlDataReader sdr;
            SqlCommand cmd = new SqlCommand();
            DataTable dtProducts = new DataTable("VendorProducts");

            try
            {
                
                cmd.CommandText = "SELECT * FROM Products WHERE ProductID IN (SELECT ProductID FROM Vendor_Product WHERE VendorID=@vendorID)";
                cmd.Parameters.Add("@vendorID", SqlDbType.Int);
                cmd.Parameters["@vendorID"].Value = vendorID;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();

                sdr = cmd.ExecuteReader();
                dtProducts.Load(sdr);

                con.Close();
                return dtProducts;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public int InsertVendorProduct(string Name, string Desc, int Price, int VendorID)
        {
            int ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "INSERT INTO Products (Name, [Description], DateAdded) output inserted.ProductID VALUES(@Name, @Desc, GETDATE())";

                cmd.Parameters.Add("@Name", SqlDbType.NVarChar);
                cmd.Parameters["@Name"].Value = Name;
                cmd.Parameters.Add("@Desc", SqlDbType.NVarChar);
                cmd.Parameters["@Desc"].Value = Desc;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;

                con.Open();
                ret = (int)cmd.ExecuteScalar();

                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();
                InsertVendorProductAssoc(VendorID, ret);
            }
            catch (Exception)
            { throw; }


            return ret;
        }


        private int InsertVendorProductAssoc(int VendorID, int ProductID)
        {
            int ret;
            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd1 = new SqlCommand();
            cmd1.CommandText = "INSERT INTO Vendor_Product (VendorID, ProductID) VALUES(@vendor, @product)";

            cmd1.Parameters.Add("@vendor", SqlDbType.Int);
            cmd1.Parameters["@vendor"].Value = VendorID;
            cmd1.Parameters.Add("@product", SqlDbType.Int);
            cmd1.Parameters["@product"].Value = ProductID;
            cmd1.CommandType = CommandType.Text;
            cmd1.Connection = con;

            try
            {
                con.Open();
                ret = cmd1.ExecuteNonQuery();

                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();
            }
            catch { throw; }

            return ret;
        }

        #endregion

        private string ConnectionString
        {
            get;
            set;
        }

    }
}