﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Flier.Startup))]
namespace Flier
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
